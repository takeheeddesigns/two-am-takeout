package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

func (suite *ServerSuite) TestHealthCheck() {
	// checks if connnection to server is good
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	respString := string(respData)
	// json object held { OK: true} when visiting server address
	// error says bind: address already in use
	// eventually I inferred that maybe I should kill the server before running the test
	// test result updated to
	// main_test.go:57:
	// Error Trace:	main_test.go:57
	// Error:      	"{\"OK\":true}\n" does not contain "\"ok\":true"
	// Test:       	TestServerSuite/TestHealthCheck
	// changed ok to OK
	assert.Contains(suite.T(), respString, "\"OK\":true")
}

func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// checking that text formatted messages go through and can be read correctly
	// the frontend seems to be sending a json object to the backend
	// test seems to check if the front end is sending a string data type to the backend
	// maybe I should check for a JSON data type in this test suite instead of a string
	// or make it so that the frontend is sending a string data type to match the test suite
	sender.WriteMessage(websocket.TextMessage, []byte("Hello, Websocket!"))

	_, message, err := reader.ReadMessage()
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), "Hello, Websocket!", string(message))
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}
