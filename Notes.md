This file is for taking notes as you go.

Challenge #1 - There is a test for the backend that is failing, fix the code so it passes.

Running the test suite showed me an error message saying bind: address already in use.
Eventually I realized that having the server active was causing this error. 
so killing the server caused the test to render a different error message which was  "{\"OK\":true}\n" does not contain "\"ok\":true".
therefore I made the appropriate syntax change to pass the tests for TestHealthCheck.

Challenge #2 - There is a test for the backend where the assumptions about what needs to be tested is incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.

This challenge was trickier. From what I gathered the test is checking if the frontend is sending a string data type to the backend.
However, it seems like the frontend is sending a json object to the backend not a string.
My assumption is that the backend should be testing the correct data type that the frontend is sending to the backend.

Challenge #3 - In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

Please view Chat.vue and App.vue for a more detailed perspective on my approach to solving this problem. 
In general, I wanted a quick simple approach. In order to display data that is given by the user, I had to create an input field to allow the user to create that data. Storing that data in local state and then somehow passing that data through the socket configuration was my general approach. Along the way, I discovered that I needed to prepackage the message object with the current user name so I could properly align messages to the left or right dependent on conditional logic that executes a combo between unique uuid's and the current user names on the client side. Said logic would appropriately position all incoming messages with their appropriate usernames to the left side of the screen. Meanwhile origin messages, messages created by the main user, would be positioned on the right side of the screen. 

Bonus Challenge - Make the frontend look nicer.

Working with Vue.js was a real treat. I think I enjoy the Vue development process a lot more than React. Styling with view wasn't too hard. I worked on,  dynamically positioning my input text field in the center of the header bar so that it remains in the center no matter the window size. And, center all components in the main body. Also, I added a scrolling effect so that the header would disappear on scroll down but reappear on a short scroll up. 

Bonus Challenge - There is a security issue in the socket server. Identify and fix it. (Hint, it's not a problem in the health check).

I couldn't identify an immediate security issue given the time I alloted myself. But, I would suspect that sensitive data isn't properly encoded to protect user's from injection attacks Packages such as gorilla/csrf would help aid in this.

